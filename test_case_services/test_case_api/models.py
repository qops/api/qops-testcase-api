########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref

from qops_tablet import db

from ..mod_configuration.models import Configuration

class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key = True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


TestCaseSteps = db.Table('test_case_steps',
                         db.Column('id', db.Integer, primary_key=True),
                         db.Column('test_case_id', db.Integer, db.ForeignKey('test_case.id'), primary_key = True),
                         db.Column('test_step_id', db.Integer, db.ForeignKey('test_step.id'), primary_key = True),
                         db.Column('sequence', db.Integer),
                         info={'bind_key': 'qops'})


TestEnvironmentConfigurations = db.Table('test_environment_configurations',
                              db.Column('id', db.Integer, primary_key=True),
                              db.Column('environment_id', db.Integer, db.ForeignKey('test_environment.id'), primary_key=True),
                              db.Column('configuration_id', db.Integer, db.ForeignKey('configuration.id'), primary_key=True),
                              info={'bind_key': 'qops'})


class TestCase(Base):
    __tablename__ = 'test_case'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    goal = db.Column(db.String)
    kind_id = db.Column(db.Integer, db.ForeignKey('test_case_kind.id'))
    requirement_id = db.Column(db.Integer, db.ForeignKey('requirement.id'))
    is_reviewed = db.Column(db.String)
    is_approved = db.Column(db.String)
    author_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    owner_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    state_id = db.Column(db.Integer, db.ForeignKey('test_case_state.id'))
    setup = db.Column(db.String)
    teardown = db.Column(db.String)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))

    test_case_steps = db.relationship('TestStep', secondary=TestCaseSteps, lazy='subquery',
                            backref=db.backref('test_cases', lazy=True))

    def get_next_identifier():
        return 'TC00001'


class TestStep(Base):
    __tablename__ = 'test_step'

    identifier = db.Column(db.String)
    setup = db.Column(db.String)
    instruction = db.Column(db.String)
    teardown = db.Column(db.String)
    expected_result = db.Column(db.String)


class TestEnvironment(Base):
    __tablename__ = 'test_environment'

    configurations = db.relationship('Configuration', secondary=TestEnvironmentConfigurations, lazy='subquery', backref=db.backref('test_environments', lazy=True))
#   test_executions = db.relationship('TestCaseExecution', secondary=???, lazy='subquery', backref=db.backref('environment', lazy=True))

    
class TestCaseExecution(Base):
    __tablename__ = 'test_case_execution'

    test_case_id = db.Column(db.Integer, db.ForeignKey('test_case.id'))
    environment_id = db.Column(db.Integer, db.ForeignKey('test_environment.id'))
    begin_date = db.Column(db.DateTime)
    complete_date = db.Column(db.DateTime)
    executed_by = db.Column(db.String)
    result_state = db.Column(db.Integer)
    result_comment = db.Column(db.String)
    steps = db.relationship('StepExecution', backref='test_case')


class StepExecution(Base):
    __tablename__ = 'step_execution'

    test_case_execution_id = db.Column(db.Integer, db.ForeignKey('test_case_execution.id'))
    test_step_id = db.Column(db.Integer, db.ForeignKey('test_step.id'))
    begin_date = db.Column(db.DateTime)
    complete_date = db.Column(db.DateTime)
    actual_result_state = db.Column(db.Integer)
    actual_result_comment = db.Column(db.String)


class StepExecutionState(Base):
    __tablename__ = 'step_execution_state'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    usage = db.Column(db.String)


class TestCaseExecutionState(Base):
    __tablename__ = 'test_case_execution_state'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    usage = db.Column(db.String)


class TestCaseKind(Base):
    __tablename__ = 'test_case_kind'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    usage = db.Column(db.String)


class TestCaseState(Base):
    __tablename__ = 'test_case_state'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    usage = db.Column(db.String)


class TestCaseReview(Base):
    __tablename__ = 'test_case_review'

    person_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    is_reviewed = db.Column(db.Boolean)
    date_reviewed = db.Column(db.DateTime)
    review_comment = db.Column(db.String)
    
