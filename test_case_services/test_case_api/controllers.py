########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
    flash, g, session, redirect, url_for

from sqlalchemy.orm import aliased

# Import models
from .models import TestCase
from .models import TestStep
from .models import TestCaseState
from .models import TestCaseKind

from ..mod_person.models import Person
from ..mod_product.models import Product
from ..mod_requirement.models import Requirement

from .forms import TestCaseProfileForm

import logging
logger = logging.getLogger('qops')

# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_test_case = Blueprint('test_case', __name__, url_prefix='/testcase')


@mod_test_case.context_processor
def store():
    store_dict = {'serviceName': 'Test Case',
                  'serviceDashboardUrl': url_for('test_case.dashboard'),
                  'serviceBrowseUrl': url_for('test_case.browse'),
                  'serviceNewUrl': url_for('test_case.new'),
                  }
    return store_dict

# Set the route and accepted methods


@mod_test_case.route('/', methods=['GET'])
def test_case():
    return render_template('test_case/test_case_dashboard.html')


@mod_test_case.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('test_case/test_case_dashboard.html')


@mod_test_case.route('/browse', methods=['GET'])
def browse():
    owner_tbl = aliased(Person, name='owner_tbl')
    author_tbl = aliased(Person, name='author_tbl')
    test_cases = TestCase.query.with_entities(TestCase.id, TestCase.identifier, TestCase.name, TestCaseKind.name.label('kind_id'), Requirement.name.label('requirement_id'), author_tbl.first_name.label('author_id'), owner_tbl.first_name.label('owner_id'), TestCaseState.name.label('test_case_state_id'), Product.name.label('product_id')).join(
        Product, TestCase.product_id == Product.id).join(TestCaseKind, TestCase.kind_id == TestCaseKind.id).join(Requirement, TestCase.requirement_id == Requirement.id).join(author_tbl, TestCase.author_id == author_tbl.id).join(owner_tbl, TestCase.owner_id == owner_tbl.id).join(TestCaseState, TestCase.state_id == TestCaseState.id)

    return render_template('test_case/test_case_browse.html', test_cases=test_cases)


@mod_test_case.route('/new', methods=['GET', 'POST'])
def new():
    test_case = TestCase()
    form = TestCaseProfileForm(request.form)
    form.state_id.choices = [(s.id, s.name) for s in TestCaseState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]
    form.kind_id.choices = [(k.id, k.name) for k in TestCaseKind.query.all()]
    form.requirement_id.choices = [(r.id, r.name)
                                   for r in Requirement.query.all()]
    form.product_id.choices = [(p.id, p.name) for p in Product.query.all()]

    if request.method == 'POST':
        form.populate_obj(test_case)
        test_case.identifier = TestCase.get_next_identifier()
        db.session.add(test_case)
        db.session.commit()
        return redirect(url_for('test_case.browse'))
    return render_template('test_case/test_case_new.html', test_case=test_case, form=form)


@mod_test_case.route('/profile', methods=['GET', 'POST'])
@mod_test_case.route('/profile/<int:test_case_id>/profile', methods=['GET', 'POST'])
def profile(test_case_id=None):
    test_case = TestCase.query.get(test_case_id)
    form = TestCaseProfileForm(obj=test_case)
    form.state_id.choices = [(s.id, s.name) for s in TestCaseState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]
    form.kind_id.choices = [(k.id, k.name) for k in TestCaseKind.query.all()]
    form.requirement_id.choices = [(r.id, r.name)
                                   for r in Requirement.query.all()]
    form.product_id.choices = [(p.id, p.name) for p in Product.query.all()]

    if request.method == 'POST':
        form = TestCaseProfileForm(request.form)
        form.populate_obj(test_case)
        db.session.add(test_case)
        db.session.commit()
        return redirect(url_for('test_case.browse'))
    return render_template('test_case/test_case_profile.html', test_case=test_case, form=form)


@mod_test_case.route('/view', methods=['GET', 'POST'])
@mod_test_case.route('/view/<int:test_case_id>/view', methods=[
    'GET', 'POST'])
def test_case_view(test_case_id=None):
    #test_case = TestCase.query.get(test_case_id)
    form = TestCaseProfileForm(obj=test_case)
    form.state_id.choices = [(s.id, s.name) for s in TestCaseState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]
    form.kind_id.choices = [(k.id, k.name) for k in TestCaseKind.query.all()]
    form.requirement_id.choices = [(r.id, r.name)
                                   for r in Requirement.query.all()]
    form.product_id.choices = [(p.id, p.name) for p in Product.query.all()]

    if request.method == 'POST':
        form = TestCaseProfileForm(request.form)
        form.populate_obj(test_case)
        db.session.add(test_case)
        db.session.commit()
        return redirect(url_for('test_case.browse'))
    return render_template('test_case/test_case_view.html',
                           test_case=test_case, form=form)


@mod_test_case.route('/profile/dashboard', methods=['GET'])
@mod_test_case.route('/profile/<int:test_case_id>/dashboard', methods=['GET'])
def test_case_dashboard(test_case_id=None):
    if test_case_id:
        test_case = TestCase.query.get(test_case_id)
    else:
        test_case = None
    return render_template('test_case/test_case_single_dashboard.html', test_case=test_case)


@mod_test_case.route('/profile/tasks', methods=['GET'])
@mod_test_case.route('/profile/<int:test_case_id>/tasks', methods=['GET'])
def test_case_tasks(test_case_id=None):
    if test_case_id:
        test_case = TestCase.query.get(test_case_id)
    else:
        test_case = None
    return render_template('test_case/test_case_single_tasks.html', test_case=test_case)


@mod_test_case.route('/profile/communication', methods=['GET', 'POST'])
@mod_test_case.route('/profile/<int:test_case_id>/communication', methods=['GET'])
def test_case_communication(test_case_id=None):
    if test_case_id:
        test_case = TestCase.query.get(test_case_id)
    else:
        test_case = None
    return render_template('test_case/test_case_single_communication.html', test_case=test_case)


@mod_test_case.route('/profile/documents', methods=['GET', 'POST'])
@mod_test_case.route('/profile/<int:test_case_id>/documents', methods=['GET'])
def test_case_documents(test_case_id=None):
    if test_case_id:
        test_case = TestCase.query.get(test_case_id)
    else:
        test_case = None
    return render_template('test_case/test_case_single_documents.html', test_case=test_case)


@mod_test_case.route('/profile/builds', methods=['GET', 'POST'])
@mod_test_case.route('/profile/<int:test_case_id>/builds', methods=['GET'])
def test_case_builds(test_case_id=None):
    if test_case_id:
        test_case = TestCase.query.get(test_case_id)
    else:
        test_case = None
    return render_template('test_case/test_case_single_builds.html', test_case=test_case)


@mod_test_case.route('/profile/requirements', methods=['GET', 'POST'])
@mod_test_case.route('/profile/<int:test_case_id>/requirements', methods=['GET'])
def test_case_requirements(test_case_id=None):
    if test_case_id:
        test_case = TestCase.query.get(test_case_id)
    else:
        test_case = None
    return render_template('test_case/test_case_single_requirements.html', test_case=test_case)


@mod_test_case.route('/profile/customers', methods=['GET', 'POST'])
@mod_test_case.route('/profile/<int:test_case_id>/customers', methods=['GET'])
def test_case_customers(test_case_id=None):
    if test_case_id:
        test_case = TestCase.query.get(test_case_id)
    else:
        test_case = None
    return render_template('test_case/test_case_single_customers.html', test_case=test_case)


@mod_test_case.route('/profile/test-cases', methods=['GET', 'POST'])
@mod_test_case.route('/profile/<int:test_case_id>/test-cases', methods=['GET'])
def test_case_test_cases(test_case_id=None):
    if test_case_id:
        test_case = TestCase.query.get(test_case_id)
    else:
        test_case = None
    return render_template('test_case/test_case_single_test_cases.html', test_case=test_case)


@mod_test_case.route('/profile/locations', methods=['GET', 'POST'])
@mod_test_case.route('/profile/<int:test_case_id>/locations', methods=['GET'])
def test_case_locations(test_case_id=None):
    if test_case_id:
        test_case = TestCase.query.get(test_case_id)
    else:
        test_case = None
    return render_template('test_case/test_case_single_locations.html', test_case=test_case)


@mod_test_case.route('/profile/steps', methods=['GET', 'POST'])
@mod_test_case.route('/profile/<int:test_case_id>/steps', methods=['GET', 'POST'])
def test_case_steps(test_case_id=None):
    if test_case_id:
        test_case = TestCase.query.get(test_case_id)
    else:
        test_case = None
    return render_template('test_case/test_case_single_steps.html', test_case=test_case)


@mod_test_case.route('add_test_steps/<int:test_case_id>', methods=['GET', 'POST'])
def test_case_add_steps(test_case_id):
    return '0'
